// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user data
const userData =  require("../data")
// Import user middleware
const userMiddleware =  require("../middlleware/middleware")

// Khai báo APi dạng Get "/" sẽ chạy vào đây
console.log(userData)
router.get("/",(request, response) => {
    response.status(200).json({
        message:"test"

    })
})

router.get("/users",userMiddleware.getAllUerMiddleware,(request, response) => {
   let age = request.query.age
   console.log(age)
     if(!age){
        response.status(200).json({
            users: userData
        }) 
     }
    if(age) {
        // Khởi tạo biến userListResponse là kết quả tìm được 
        let userListResponse = [];

        // Duyệt từng phần tử của mảng để tìm được user có age tương ứng
        for (let index = 0; index < userData.length; index++) {

            let userElement = userData[index];

            console.log(userElement.age)
        if(userElement.age > age ){

            userListResponse.push(userElement);
        
        }
        };
        response.status(200).json({
            users: userListResponse
        })
    } 
})

router.get("/users/:userId", userMiddleware.getAllUerByIdMiddleware,(request, response) => {
    let UserId = request.params.userId;
    // Do UserId nhận được từ params là string nên cần chuyển thành kiểu integer
    UserId = parseInt(UserId); 
     console.log(UserId)
// Khởi tạo biến userResponse là kết quả tìm được 
     let userResponse = null;

   if(isNaN(UserId)){
    response.status(400).json({
        status:" id không hợp lệ"
    })
   }
   else{
    
   
    // Duyệt từng phần tử của mảng để tìm được user có Id tương ứng
    for (let index = 0; index < userData.length; index++) {
        let userElement = userData[index];

        if(userElement.checkUserId(UserId)) {
            userResponse = userElement;
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
        
    };

    response.status(200).json({
        users: userResponse
    })
   }
    
})

module.exports = router
   
