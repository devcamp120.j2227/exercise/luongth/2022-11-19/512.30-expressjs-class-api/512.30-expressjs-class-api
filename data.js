class  User{
    constructor(paramId,paramName,paramPosition,paramOffice,paramAge,paramStartDate){
        this.id = paramId
        this.name = paramName;
        this.position = paramPosition;
        this.office = paramOffice;
        this.age = paramAge;
        this.startDate = paramStartDate
    }
    //validate paramId
    checkUserId(paramId) {
       return this.id === paramId
    }
    

}
let userClassList = [];
//khoởi tạo các class user
let AiriSatouClass = new User(1,"AiriSatou","Accountant","Tokyo",33,"2008/11/28");
userClassList.push(AiriSatouClass);

let AngelicaClass = new User(2,"Angelica Ramos","Chief Executive Officer (CEO)","London",47,"2009/10/09")

userClassList.push(AngelicaClass);

let Ashton = new User(3,"Ashton Cox","Junior Technical Author","San Francisco",66,"2009/01/12");
userClassList.push(Ashton);

let Bradley = new User(4,"Bradley Greer","Software Engineer","London",41,"2012/10/13");
userClassList.push(Bradley);

let BrendenWagner = new User(5,"Brenden Wagner","Software Engineer","San Francisco",28,"2011/06/07");
userClassList.push(BrendenWagner);

let BrielleWilliamson = new User(6,"Brielle Williamson","ntegration Specialist","New York",61,"2012/12/02");
userClassList.push(BrielleWilliamson);

let BrunoNash= new User(7,"Bruno Nash","Software Engineer","London",38,"2011/05/03");
userClassList.push(BrunoNash);

let CaesarVance= new User(8,"Caesar Vance","Caesar Vance","New York",21,"2011/12/12");
userClassList.push(CaesarVance);

let CaraStevens= new User(9,"Cara Stevens","Sales Assistant","New York",46,"2011/12/06");
userClassList.push(CaraStevens);

let CedricKelly= new User(10,"Cedric Kelly","Senior Javascript Developer","Edinburgh",22,"2012/03/29");
userClassList.push(CedricKelly);

console.log(userClassList)
module.exports = userClassList

    