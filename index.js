const { request, response } = require("express");
const express = require("express"); // Tương tự : import express from "express";

// Khai báo router app
const userRouter = require("./router/router");
// Khởi tạo Express App
const app = express();

const port = 8000;

// Cấu hình để API đọc được body JSON
app.use(express.json());

// Cấu hình để API đọc được body có ký tự tiếng Việt
app.use(express.urlencoded({
    extended: true
}))

// App sử dụng router
app.use("/api", userRouter);


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
